package android.kimmy.annoprocessordemo;

import android.kimmy.xlannoprocessor.route.XLRouteAnno;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


@XLRouteAnno(value = "helloWorld", prefix = 1)
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
