package android.kimmy.annoprocessordemo;

import android.app.Application;
import android.kimmy.xlannoprocessor.route.IRouteContentProvider;
import android.kimmy.xlannoprocessor.route.XLRouteAnnoHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能：
 * 描述：
 * Created by 陈俊杰 on 2018/7/19.
 */
public class App extends Application {
    Map<String, Class<?>> routeMap = new HashMap<>();


    @Override
    public void onCreate() {
        super.onCreate();
        initRoute("app");
    }


    void initRoute(String... moduleNames) {

        for (String moduleName : moduleNames) {
            String path = XLRouteAnnoHelper.getProviderImplPath(moduleName);
            try {
                Class cls = Class.forName(path);
                IRouteContentProvider routeContentProvider = (IRouteContentProvider) cls.newInstance();
                routeContentProvider.handleRoute(routeMap);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {

            }
        }

        Log.d("测试", routeMap.size() + "");

    }
}
